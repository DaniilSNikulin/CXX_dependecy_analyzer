#ifndef PLUGINACTION_H
#define PLUGINACTION_H


#include <memory>
#include <clang/AST/ASTConsumer.h>
#include <clang/Frontend/CompilerInstance.h>
#include "clang/Frontend/FrontendPluginRegistry.h"


class myPlugin : public clang::PluginASTAction
{
protected:
	bool ParseArgs(const clang::CompilerInstance &CI, const std::vector<std::string>& args) override;
#if ((__clang_major__ == 3) && (__clang_minor__ >= 6))
	std::unique_ptr<clang::ASTConsumer> CreateASTConsumer (clang::CompilerInstance &, llvm::StringRef) override;
#else
	clang::ASTConsumer* CreateASTConsumer(clang::CompilerInstance &, llvm::StringRef) override;
#endif

	std::string MainFile;
private:
	std::map<std::string, std::vector<std::string> > Arguments;
};


#endif /* PLUGINACTION_H */
