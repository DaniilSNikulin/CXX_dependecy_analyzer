
#include <fstream>
#include <iostream>
#include <utility>
#include <string>
#include <map>
#include <vector>
#include <getopt.h>
#include <unistd.h>
#include <memory>
#include <cstring>
#include <cstdlib>



std::vector<const char*> parse_conf (char const * const file);  //TODO: ну и пусть себе течет
void print_help ();


struct MyArgv
{
	char ** argv;
	size_t byte_capacity;
	int argc;

	MyArgv(const std::vector<const char*> &vect_param)
		: argc(vect_param.size()),
		  byte_capacity(0)
	{
		for (size_t i = 0; i < vect_param.size(); ++i)
		{
			byte_capacity += strlen(vect_param[i]) + 1;
		}

		char * chunks = (char *) malloc(byte_capacity);
		argv = (char**) malloc(sizeof(char*) * argc);
		if (chunks == NULL)
		{
			printf ("argv == NULL; (ArgsParser::MyArgv::constructor)\n\n");
			exit(1);
		}

		byte_capacity = 0;
		size_t local_capacity = 0;
		for (size_t i = 0; i < vect_param.size(); ++i)
		{
			local_capacity = strlen(vect_param[i]);
			memcpy(chunks + byte_capacity, vect_param[i], local_capacity);
			argv[i] = chunks + byte_capacity;
			byte_capacity += local_capacity;
			chunks[++byte_capacity] = '\0';
			size_t diff = strlen(argv[i]) - local_capacity;
			if (diff > 0)
			{
				argv[i][strlen(argv[i])-diff] = '\0';
			}
		}
	}

	~MyArgv()
	{
		free(argv);
	}
};


std::map<std::string, std::vector<std::string> > ParseArgs (std::vector<const char*> &vect_param)
{
	std::map<std::string, std::vector<std::string> > map_options;

	if (vect_param.size() == 0)
	{
		return map_options;
	}

	static char const * const output = "output";
	static char const * const verbose = "verbose";
	static char const * const config = "config";
	static char const * const proxy = "proxy";
	static char const * const proxypath = "proxypath";

	static const struct option long_options[] = {
		{ "help", no_argument, NULL, 'h' },
		{ output, no_argument, NULL, 'o' },
		{ verbose, no_argument, NULL, 'v' },
		{ config, required_argument, NULL, 0 },
		{ proxy, required_argument, NULL, 0 },
		{ proxypath, required_argument, NULL, 0 },
		{ NULL, no_argument, NULL, 0 }
	};
	static char const * const short_options = "hvo:0";

	int opt = 0;
	int option_index = -1;

	MyArgv args (vect_param);


	while (true)
	{
		opt = getopt_long(args.argc, args.argv, short_options, long_options, &option_index);
		if (opt == -1)
		{
			break;
		}

		switch (opt)
		{
		case 'h':
		{
			print_help();
			break;
		}
		case 'o':
		{
			if (map_options.find(output) == map_options.end())
			{
				map_options[verbose].push_back(optarg);
			}
			break;
		}
		case 'v':
		{
			map_options[verbose];
			break;
		}
		case 0 :
		{
			if (0 == strcmp(long_options[option_index].name, proxy))
			{
				map_options[proxy].push_back(optarg);
			}
			if (0 == strcmp(long_options[option_index].name, proxypath))
			{
				map_options[proxypath].push_back(optarg);
			}
			if (0 == strcmp(long_options[option_index].name, config))
			{
				std::vector<const char*> conf_argv = parse_conf(optarg);
				auto conf_option = ParseArgs(conf_argv);
				if ((conf_option.find(verbose)) != (conf_option.end()))
				{
					map_options[verbose];
				}
				auto conf_proxy_it = conf_option.find(proxy);
				if (conf_proxy_it != (conf_option.end()))
				{
					std::vector<std::string> & proxy_files = map_options[proxy];
					proxy_files.insert(proxy_files.end(), conf_proxy_it->second.begin(), conf_proxy_it->second.end());
				}
				auto conf_proxypath_it = conf_option.find(proxypath);
				if (conf_proxypath_it != (conf_option.end()))
				{
					std::vector<std::string> & proxypath_files = map_options[proxypath];
					proxypath_files.insert(proxypath_files.end(),
					                       conf_proxypath_it->second.begin(), conf_proxypath_it->second.end());
				}
				return map_options;
			}
			break;
		}
		}

		option_index = -1;
	}

	// for (auto it = map_options.begin(); it != map_options.end(); ++it)
	// {
	// 	std::cout << it->first << "\n";
	// 	if (it->second.size())
	// 	{
	// 		for (auto vect_it = it->second.begin(); vect_it != it->second.end(); ++vect_it)
	// 		{
	// 			std::cout << "   " << *vect_it << '\n';
	// 		}
	// 	}
	// }


	return map_options;
}


std::vector<const char*> parse_conf (char const * const file)
{
	std::ifstream config {file};
	std::vector<const char*> commands;
	if (!config)
	{
		printf("error in open config file\n");
		return commands;
	}
	char* str = new char[16];			//
	strcpy(str, "./trash.exe");		//
	commands.push_back(str);			//
	str = new char[16];						//
	strcpy(str, "--trash");				//
	commands.push_back(str);			/// don't touch. duct tape
	str = new char[16];						//
	strcpy(str, "./trash.exe");		//
	commands.push_back(str);			//
	while (config)
	{
		std::string param;
		config >> param;
		if (param == "\\")
		{
			continue;
		}
		char* str = new char[param.size()+1];
		strcpy(str, param.c_str());
		commands.push_back (str);
	}

	return commands;
}


void print_help ()
{
	std::cout << "-h [ --help ]       " << "Show help\n"
	          << "-v [ --verbose ]    " << "Verbose output\n"
	          << "-c [ --config ]     " << "Config file with flags for compilation\n\n"
	          << "   [ --proxy ]      " << "frontend file for inclusion\n"
	          << "   [ --proxypath ]  " << "path, where all files are proxy\n";
}


// int main (int ac, const char** av)
// {
// 	std::vector<const char*> vect;

// 	for (int i = 0; i < ac; ++i)
// 	{
// 		vect.push_back(*(av+i));
// 	}

// 	std::map<std::string, std::vector<std::string> > map_options = ParseArgs(vect);

// 	for (auto it = map_options.begin(); it != map_options.end(); ++it)
// 	{
// 		std::cout << it->first << "\n";
// 		if (it->second.size())
// 		{
// 			for (auto vect_it = it->second.begin(); vect_it != it->second.end(); ++vect_it)
// 			{
// 				std::cout << "   " << *vect_it << '\n';
// 			}
// 		}
// 	}
// }
