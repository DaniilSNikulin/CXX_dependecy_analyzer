#ifndef GRAPHHANDLER_H
#define GRAPHHANDLER_H

//#include "AbstractHandler.hpp"
#include "graph_type.hpp"
#include <iosfwd>
#include <string>

class GraphHandler
{
public:
	GraphHandler (std::ostream &out, const string &MainFile);
	const graph_type & getGraph();
	void AddLink (const string &name1, const string &name2);
	void decrIncl (const string &file);

	void setLinesNumber (const string & file, int first, int last);
	bool AddGuard (const string & file, const string & MacroName);
	void EraseGuard (const string & file, const string & MacroName);
	bool insertPragmaLoc (const string &file, size_t line);
	void ErasePragma (const string &file, size_t line);

	void Print ();
	void MinimizeGuard (const std::map<string, std::pair<int, int> > & LinesNumberMap);

private:
	std::ostream &out;
	std::string MainFile;
	graph_type file_graph;
	std::map<std::pair<string, string>, int > MltIncl;
	std::map<string, int> DecrMltIncl;

	std::map<string, string> hg_map;
	std::map<string, std::set<size_t> > AnyPragmaLoc;
	std::map<string, std::pair<int, int> > LinesNumberMap;

	void MinimizeMltIncl ();
	void Draw ();
	void SubDraw (string file);
	inline string getColor (string file);
	inline string getShape (string file);
};



#endif /* GRAPHHANDLER_H */
