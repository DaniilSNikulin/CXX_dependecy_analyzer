#ifndef MYVISITOR_H
#define MYVISITOR_H


#include "DependencyHandler.hpp"
#include <memory>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>
#include <clang/AST/DeclBase.h>
#include <clang/AST/DeclTemplate.h>
#include <clang/AST/Expr.h>
#include <clang/Basic/SourceManager.h>


class MyASTVisitor : public clang::RecursiveASTVisitor<MyASTVisitor>
{
public:
	MyASTVisitor (clang::SourceManager &SM, std::unique_ptr<DependencyHandler> vp_DependencyHandler);
	std::unique_ptr<DependencyHandler> releaseDpnd ();
	bool VisitDeclContext (clang::DeclContext *DC);

	bool VisitCXXRecordDecl (clang::CXXRecordDecl *D);
	bool VisitDeclaratorDecl (clang::DeclaratorDecl *D);
	bool VisitFunctionDecl (clang::FunctionDecl *D);
	bool VisitCXXMethodDecl (clang::CXXMethodDecl *D);
	bool VisitClassTemplateSpecializationDecl (clang::ClassTemplateSpecializationDecl *D);
	bool VisitTypedefNameDecl (clang::TypedefNameDecl *D);
	bool VisitCallExpr (clang::CallExpr *E);
	bool VisitExplicitCastExpr (clang::ExplicitCastExpr *E);

private:
	clang::SourceManager & _SM;
	std::unique_ptr<DependencyHandler> vp_DependencyHandler;
};


#endif /* MYVISITOR_H */
