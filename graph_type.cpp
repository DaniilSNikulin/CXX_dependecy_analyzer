
#include "graph_type.hpp"
#include "FreeFunctions.hpp"
#include <algorithm>
#include <iostream>
#include <iomanip>

using namespace std;

set<string> graph_type::Shadow;


void graph_type::CheckCycle (const string &file)
{
	Before.clear ();
	vector<string> Stack;
	Stack.reserve (100);
	SubCheckCycle (file, Stack);
}


// private:

void graph_type::SubCheckCycle (const string &file, vector<string> &Stack)
{
	graph_type::const_iterator file_iter;
	if (end() == (file_iter = find(file)))
	{
		return;
	}
	if (std::find (Stack.begin(), Stack.end(), file) != Stack.end())
	{
		if (!isShadow(file))
		{
			cerr << "find cycle :\n"<< setw(60) << file << endl;
			for_each (Stack.rbegin(), Stack.rend(), [](string name){cerr << setw(60) << name << '\n';});
			cerr << endl;
		}
		auto from_iter = find (Stack.back());
		from_iter->second.erase (file);
		return;
	}

	if (!Before.insert(file).second)
	{
		return;
	}

	Stack.push_back (file);
	auto sub_iter = file_iter->second.begin();
	while (sub_iter != file_iter->second.end())
	{
		SubCheckCycle (*(sub_iter++), Stack);
	}
	Stack.pop_back();
}


// free functions

bool isShadow (string path)
{
	for (string sh_path : graph_type::Shadow)
	{
		if (path.find(sh_path) != string::npos) {
			return true;
		}
	}
	return false;
}

