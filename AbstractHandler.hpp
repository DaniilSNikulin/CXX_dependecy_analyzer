#ifndef ABSTRACTHANDLER_H
#define ABSTRACTHANDLER_H

// #include <libxml++/libxml++.h>
// #include <libxml++/document.h>

namespace clang
{
	class DiagnosticsEngine;
}

class AbstractHandler
{
public:
	AbstractHandler (clang::DiagnosticsEngine & DiagEng) : DiagEng (DiagEng) {}
	virtual ~AbstractHandler () {}
	virtual void Print () {}
protected:
	clang::DiagnosticsEngine & DiagEng;
};


#endif /* ABSTRACTHANDLER_H */
