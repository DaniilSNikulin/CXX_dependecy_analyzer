
#include "GraphHandler.hpp"
#include "FreeFunctions.hpp"
#include <iostream>
#include <iomanip>

using namespace std;

GraphHandler::GraphHandler (std::ostream &out, const string &MainFile)
	: out (out), MainFile(MainFile)
{
}

void GraphHandler::AddLink (const string &name1, const string &name2)
{
	if ((!file_graph[name1].insert(name2).second) && (!isShadow(name1)))
	{
		auto file_pair = make_pair (name1, name2);
		MltIncl[file_pair]++;
	}
}

void GraphHandler::decrIncl (const string &file)
{
	if (!isShadow (file))
	{
		DecrMltIncl[file]++;
	}
}


const graph_type & GraphHandler::getGraph ()
{
	static bool first = true;
	if (first)
	{
		first = false;
		file_graph.CheckCycle (MainFile);
	}
	return file_graph;
}

void GraphHandler::setLinesNumber (const string &file, int first, int last)
{
	if ((LinesNumberMap[file].first == 0)
	    || ((LinesNumberMap[file].first > first)
	        && (LinesNumberMap[file].second < last)))
	{
		LinesNumberMap[file].first = first;
		LinesNumberMap[file].second = last;
	}
}

bool GraphHandler::AddGuard (const string & file, const string &MacroName)
{
	auto iter = hg_map.find(file);
	if ((iter != hg_map.end()) && (iter->second.size()))
	{
		return false;
	}
	hg_map[file] = MacroName;
	return true;
}

void GraphHandler::EraseGuard (const string & file, const string & MacroName)
{
	auto iter = hg_map.find (file);
	if ((iter != hg_map.end()) && (iter->second == MacroName))
	{
		hg_map.erase (iter);
	}
}

bool GraphHandler::insertPragmaLoc (const string &file, size_t line)
{
	return AnyPragmaLoc[file].insert (line).second;
}

void GraphHandler::ErasePragma (const string &file, size_t line)
{
	auto iter = AnyPragmaLoc.find (file);
	if (iter == AnyPragmaLoc.end())
	{
		printf("iter == AnyPragmaLoc.end()");
		exit(1);
	}
	iter->second.erase (line);
	if (!iter->second.size())
	{
		AnyPragmaLoc.erase (iter);
	}
}


void GraphHandler::Print ()
{
	Draw();
	MinimizeMltIncl ();
	if (MltIncl.size())
	{
		cerr << "\n\n" << setw(60) << "Multiple inclusion:\n\n";
	}
	for (auto& file_pair_pair : MltIncl)
	{
		cerr << setw(80) << file_pair_pair.first.first
		     << "     included  " << setw(80) << file_pair_pair.first.second
		     << "         " << file_pair_pair.second + 1 << " times\n";
	}
	cerr << "\n\n";

	for (auto& hg_pair : hg_map)
	{
		for (auto& sub_hg_pair : hg_map) {
			if ((!isShadow(hg_pair.first) && !isShadow(sub_hg_pair.first))
			    && (hg_pair != sub_hg_pair) && (hg_pair.second == sub_hg_pair.second))
			{
				cerr << setw(60) << hg_pair.first << "    _have the same hg as_ "
				     << setw(75) << sub_hg_pair.first
				     <<  "  _guard name:_ " << hg_pair.second << '\n';
			}
		}
	}
}


void GraphHandler::MinimizeGuard (const map<string, std::pair<int, int> > & LinesNumberMap)
{
	for (auto& ppc_el : this->LinesNumberMap)
	{
		auto astv_iter = LinesNumberMap.find(ppc_el.first);
		auto hg_iter = hg_map.find(ppc_el.first);
		if ((hg_iter != hg_map.end())
		    && (astv_iter != LinesNumberMap.end())
		    && ((astv_iter->second.first < ppc_el.second.first)
		        || (astv_iter->second.second > ppc_el.second.second)) )
		{
			hg_map.erase (hg_iter);
		}
	}
}

// private

void GraphHandler::MinimizeMltIncl ()
{
	auto iterMlt = MltIncl.begin();
	while (iterMlt != MltIncl.end())
	{
		auto iter = DecrMltIncl.find(iterMlt->first.first);
		if (iter != DecrMltIncl.end())
		{
			iterMlt->second -= iter->second;
		}
		if (iterMlt->second == -1)
		{
			iterMlt = MltIncl.erase (iterMlt);
		}
		else
		{
			++iterMlt;
		}
	}
}


void GraphHandler::Draw ()
{
	out << "digraph \" \"{\n";
	for (auto& file_el : file_graph)
	{
		if (isShadow (file_el.first))
		{
			continue;
		}
		for (auto& sub_file : file_el.second)
		{
			SubDraw (file_el.first);
			SubDraw (sub_file);
			out << "\"" << file_el.first << "\"" << "->" << "\"" << sub_file << "\""  << ";\n\n";
		}
	}
	out << "}" << flush;
}

void GraphHandler::SubDraw (string file)
{
	string label = getLabel (file);
	out << "\"" << file << "\"[label=\""
	    << label << "\", style=filled"
	    << getColor(file) << getShape(file)
	    << "];\n";
}

string GraphHandler::getColor (string file)
{
	if ( (hg_map.find(file) != hg_map.end())
	     || (AnyPragmaLoc.find(file) != AnyPragmaLoc.end()))
	{
		return ", fillcolor=white";
	}
	return ", fillcolor=yellow";
}

string GraphHandler::getShape (string file)
{
	if (isShadow(file)) {
		return ", shape=box";
	}
	return ", shape=ellipse";
}

