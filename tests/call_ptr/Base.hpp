#ifndef BASE_H
#define BASE_H

struct Base
{
	void foo ();
	virtual ~Base();
};

#endif /* BASE_H */
