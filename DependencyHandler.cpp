
#include "DependencyHandler.hpp"
#include "FreeFunctions.hpp"

#include <algorithm>
#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>
#include <clang/AST/DeclTemplate.h>
#include <clang/AST/Type.h>
#include <clang/Basic/SourceManager.h>
#include <clang/Basic/SourceLocation.h>

#include <iostream>

// using namespace std;
// using namespace clang;

const clang::SourceManager* DependencyHandler::PairLocCmp::p_SM;

static std::string duct_tape (const clang::SourceLocation &SL, const clang::SourceManager &SM)
{
	std::string S;
	llvm::raw_string_ostream OS(S);
	SL.print(OS, SM);
	return OS.str();
}

bool DependencyHandler::PairLocCmp::operator () (const DependencyHandler::PairLoc &first,
                                                 const DependencyHandler::PairLoc &second) const
{
	auto SubFirst = duct_tape(first.first, *p_SM);
	SubFirst += duct_tape(first.second, *p_SM);

	auto SubSecond = duct_tape(second.first, *p_SM);
	SubSecond += duct_tape(second.second, *p_SM);

	return SubFirst < SubSecond;
}



DependencyHandler::DependencyHandler (clang::DiagnosticsEngine &DiagEng, clang::SourceManager &SM, bool verbose)
	: AbstractHandler(DiagEng), _SM(SM), verbose(verbose)
{
	PairLocCmp::p_SM = &_SM;;
}

void DependencyHandler::setVerbose (bool new_verbose)
{
	verbose = new_verbose;
}


bool DependencyHandler::bind_dpnds (const clang::SourceLocation &Loc, const clang::NamedDecl* Decl, DpndKind what)
{
	string to, from = getCanonical (duct_tape(Loc, _SM));
	if ((!Decl) || (isShadow(from)) || (from == (to = getCanonical(duct_tape(Decl->getLocStart(), _SM)))))
	{
		return false;
	}
	if (what == DpndKind::DefKind)/* || (isShadow(to))*/
	{
		BackSideMap[Decl].insert(Loc);
		return def_dpnds[from].insert(Decl).second;
	}
	return decl_dpnds[from].insert(Decl).second;
}


void DependencyHandler::setDpndsFromType (const clang::SourceLocation &Loc,
                                          clang::Type const * const v_Type,
                                          DpndKind Kind)
{
	string from = getCanonical( duct_tape(Loc, _SM) );
	if (!v_Type || isShadow(from))
	{
		return;
	}

	const clang::CXXRecordDecl *Decl = SubSetDpndsFromType (Loc, v_Type, Kind);

	auto templSpec = dynamic_cast<const clang::ClassTemplateSpecializationDecl*> (Decl);
	if (templSpec)
	{
		for (auto& arg : templSpec->getTemplateInstantiationArgs().asArray() )
		{
			if (arg.getKind() == clang::TemplateArgument::ArgKind::Type)
			{
				// if (arg.getAsType()->getAsCXXRecordDecl())
				{
					// string tmp = getWithoutParent (arg.getAsType()->
					//                                getAsCXXRecordDecl()->
					//                                getLocation().printToString(_SM));
					// if (!isShadow(tmp))
					// {
					// 	std::cout << Loc.printToString(_SM)
					// 	          << "  " << tmp
					// 	          << "  " << Kind
					// 	          << std::endl;
					// }
					setDpndsFromType (Loc, arg.getAsType().getTypePtr(), Kind);
				}
			}
		}
	}
}


void DependencyHandler::setLinesNumber (const clang::SourceLocation &first, const clang::SourceLocation &last)
{
	string filename = getCanonical( duct_tape(first, _SM) );
	int LineNumber = getLineNumber( duct_tape(last, _SM) );
	LinesNumberMap[filename].second = std::max (LineNumber, LinesNumberMap[filename].second);
	LineNumber = getLineNumber( duct_tape(first, _SM));
	if ((LinesNumberMap[filename].first == 0)
	    || (LinesNumberMap[filename].first > LineNumber))
	{
		LinesNumberMap[filename].first = LineNumber;
	}
}

const std::map<string, std::pair<int, int> > & DependencyHandler::getLinesNumber ()
{
	return LinesNumberMap;
}


std::set<string> DependencyHandler::getDpnds (const string &file)
{
	std::set<string> DpndsSet;
	auto iter = def_dpnds.find(file);
	if (iter != def_dpnds.end())
	{
		for (const clang::NamedDecl *Decl : iter->second)
		{
			DpndsSet.insert (getCanonical( duct_tape(Decl->getLocStart(), _SM) ));
		}
	}
	for (auto &pairLoc : MacroDep)
	{
		if (getCanonicalDir( duct_tape(pairLoc.first, _SM)) == file)
		{
			DpndsSet.insert (getCanonicalDir( duct_tape(pairLoc.second, _SM)) );
		}
	}
	return DpndsSet;
}


void DependencyHandler::eraseAddledDeclDpnds (const graph_type &file_graph)
{
	for (auto& file_el : decl_dpnds)
	{
		auto iter_file = file_graph.find (file_el.first);
		auto iter_dpnds = def_dpnds.find (file_el.first);
		erase_if (file_el.second, [&](const clang::NamedDecl *Decl) -> bool
		          {
			          string to {getCanonical( duct_tape(Decl->getLocStart(), _SM))};
			          bool is_inclusion (iter_file->second.find(to) != iter_file->second.end());
			          if (is_inclusion && (iter_dpnds != def_dpnds.end()))
			          {
				          auto iter_set
						          = find_if (iter_dpnds->second.begin(), iter_dpnds->second.end(),
						                     [&to, this] (const clang::NamedDecl *Decl) -> bool
						                     {
							                     return (to == getCanonical( duct_tape(Decl->getLocStart(), _SM)));
						                     });
				          bool is_need (iter_set != iter_dpnds->second.end());
				          return is_need;
			          }
			          return !is_inclusion;
		          });
	}
	erase_if (decl_dpnds, [](DpndContainer::value_type Value) -> bool
	          {
		          return !Value.second.size();
	          });
}


void DependencyHandler::setNeedDelete (const string &file, const graph_type &ProxyMapping,
                                       const AddDeleteType &AddDelete)
{
	NeedDeleteMap[file].first = ProxyMapping;
	NeedDeleteMap[file].second = AddDelete;
}


void DependencyHandler::Print ()
{
	PrintReplacementRecomendation ();
	PrintAddDeleteRecomenadtion ();
}

// private


const clang::CXXRecordDecl* DependencyHandler::SubSetDpndsFromType (const clang::SourceLocation &Loc,
                                                                    clang::Type const * const vp_Type,
                                                                    DependencyHandler::DpndKind Kind)
{
	const clang::CXXRecordDecl *Decl {nullptr};
	if ((Decl = vp_Type->getAsCXXRecordDecl()) != 0)
	{
		if (Decl->hasDefinition())
		{
			Decl = Decl->getDefinition();
		}
		if ((!Decl->getDestructor()) || (getCanonical( duct_tape(Decl->getLocStart(), _SM) )
		                                 == getCanonical( duct_tape(Decl->getDestructor()->getLocStart(), _SM))))
		{
			bind_dpnds (Loc, Decl, Kind);
		}
		else
		{
			bind_dpnds (Loc, Decl->getDestructor(), Kind);
		}
	}
	else if ((Decl = vp_Type->getPointeeCXXRecordDecl()) != 0)
	{
		if (!bind_dpnds(Loc, Decl, DpndKind::DeclKind))
		{
			return nullptr;
		}
	}
	return Decl;
}


void DependencyHandler::PrintReplacementRecomendation ()
{
	for (auto& file_el : decl_dpnds)
	{
		for (const clang::NamedDecl* Decl : file_el.second)
		{
			auto Loc = _SM.getIncludeLoc (_SM.getFileID (Decl->getLocation()));
			string file_del {getCanonical( duct_tape(Decl->getLocation(), _SM))};
			if (isShadow(file_del))
			{
				continue;
			}
			size_t warnID =
					DiagEng.getCustomDiagID (clang::DiagnosticsEngine::Warning,
					                         "You can delete inclusion of file  %0  and add forvard declare class  %q0");
			size_t noteID = DiagEng.getCustomDiagID (clang::DiagnosticsEngine::Note, "Class %q0 declared here:");
			DiagEng.Report (Loc, warnID) << file_del << Decl;
			DiagEng.Report (Decl->getLocation(), noteID) << Decl;
			auto iter = NeedDeleteMap.find (file_el.first);
			if (iter != NeedDeleteMap.end())
			{
				iter->second.second.second.erase (file_del);
			}
		}
	}
}



void DependencyHandler::PrintAddDeleteRecomenadtion ()
{
	for (auto& PairFilePair : NeedDeleteMap)
	{
		const std::string &SourceFile = PairFilePair.first;
		auto& ProxyMapping = PairFilePair.second.first;
		auto& ForceNeed = PairFilePair.second.second.first;
		for (auto& proxy_el : ProxyMapping)
		{
			auto& file_add (proxy_el.first);
			auto& proxy_set (proxy_el.second);
			auto ForceIter (ForceNeed.find (proxy_el.first));
			if (ForceIter != ForceNeed.end())
			{
				auto def_iter (def_dpnds.find (SourceFile));
				if (def_iter != def_dpnds.end())
				{
					for (const clang::NamedDecl *Decl : def_iter->second)
					{
						if (file_add == getCanonical( duct_tape(Decl->getLocation(), _SM)))
						{
							auto& setLoc = BackSideMap[Decl];
							auto LocIter (find_if(setLoc.begin(), setLoc.end(), [&SourceFile, this](const clang::SourceLocation &Loc)
							                      {
								                      return (getCanonical( duct_tape(Loc, _SM) ) == SourceFile);
							                      }));
							size_t warnID = DiagEng.getCustomDiagID (clang::DiagnosticsEngine::Warning,
							                                         "Here you use class %q0 and need unclude %1");
							DiagEng.Report (*LocIter, warnID) << Decl << file_add;
							if (*proxy_set.begin() != file_add)
							{
								for (auto& file_proxy : proxy_set)
								{
									size_t noteID = DiagEng.getCustomDiagID (clang::DiagnosticsEngine::Note,
									                                         "You can instead include this proxy file %0");
									DiagEng.Report (noteID) << file_proxy;
								}
							}
						}
					}
				}
			}
			else if (proxy_set.size())
			{
				size_t warnID = DiagEng.getCustomDiagID
						(clang::DiagnosticsEngine::Warning, "In file %0 You use too much proxy for inclusion file %1");
				DiagEng.Report (warnID) << SourceFile << file_add;
				for (auto& file_proxy : proxy_set)
				{
					size_t noteID = DiagEng.getCustomDiagID (clang::DiagnosticsEngine::Note, "  %0");
					DiagEng.Report (noteID) << file_proxy;
				}
			}
		}

		for (auto& file_del : PairFilePair.second.second.second)
		{
			size_t warnID = DiagEng.getCustomDiagID (clang::DiagnosticsEngine::Warning,
			                                         "In File %0 you can delete %1");
			DiagEng.Report (warnID) << SourceFile << file_del;
		}
	}
}
